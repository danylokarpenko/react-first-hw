import React, { Component } from 'react';
import PropTypes from 'prop-types';

class EditMessageInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textInput: this.props.message.text
        }
    }
    componentWillMount() {
        this.setState({
            textInput: this.props.message.text
        })
    }
    onChange(e, keyword) {
        const value = e.target.value;
        this.setState({
            [keyword]: value
        })
    }

    formMessage(text) {
        const { id } = this.props.message;
        return {
            id,
            text,
            editedAt: new Date().toISOString()
        }
    }

    onClick() {
        const message = this.formMessage(this.state.textInput);
        this.props.onEdit(message);
        this.setState({
            textInput: ''
        })
    }
    render() {
        return (
            <div className="MessageInput">
                <div>
                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                        <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                    </svg>
                    Edit Message
                </div>
                <div className="input-group">
                    <textarea placeholder="Type here.." className="form-control" aria-label="With textarea" value={this.state.textInput} onChange={(e) => this.onChange(e, 'textInput')}></textarea>
                    <div className="input-group-prepend">
                        <button className="btn btn-outline-primary" type="button" onClick={() => this.onClick()}>Edit</button>
                    </div>
                </div>
            </div>
        );
    }
}

EditMessageInput.propTypes = {
    user: PropTypes.object.isRequired,
    message: PropTypes.object.isRequired,
    onEdit: PropTypes.func.isRequired
}

export default EditMessageInput;