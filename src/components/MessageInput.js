import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { v4 as uuid } from 'uuid';

class MessageInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textInput: ''
        }
    }

    onChange(e, keyword) {
        const value = e.target.value;
        this.setState({
            [keyword]: value
        })
    }
    formMessage(text) {
        const { userId, avatar, user } = this.props.user;
        return {
            id: uuid(),
            text,
            userId,
            avatar,
            user,
            reactions: [],
            createdAt: new Date().toISOString(),
            editedAt: ''
        }
    }

    onClick() {
        if (this.state.textInput.length === 0) return; 
        const message = this.formMessage(this.state.textInput);
        this.props.onSend(message);
        this.setState({
            textInput: ''
        })
    }

    render() {
        return (
            <div className="MessageInput">
                <div className="input-group">
                    <textarea placeholder="Type here.." className="form-control" aria-label="With textarea" value={this.state.textInput} onChange={(e) => this.onChange(e, 'textInput')}></textarea>
                    <div className="input-group-prepend">
                        <button className="btn btn-outline-primary" type="button" onClick={() => this.onClick()}>Send</button>
                    </div>
                </div>
            </div>
        );
    }
}

MessageInput.propTypes = {
    user: PropTypes.object.isRequired,
    onSend: PropTypes.func.isRequired,
    editMessage: PropTypes.func
}

export default MessageInput;