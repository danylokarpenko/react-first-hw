import React, { Component } from 'react';
import Spinner from './Spinner';
import Header from './Header';
import MessageList from './MessageList';
import MessageInput from './MessageInput';
import EditMessageInput from './EditMessageInput';
import PageHeader from './PageHeader';
import PageFooter from './PageFooter';
import '../App.css';
import callWebApi from '../helpers/webApiHelper';

class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            messages: [],
            name: 'My Chat',
            editMessage: null,
            user: {}
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    async fetchData() {
        const response = await callWebApi({
            endpoint: 'https://edikdolynskyi.github.io/react_sources/messages.json',
            type: 'GET'
        });
        const messages = response.map(message => Object.assign(message, { reactions: [] }))
        // get random user 
        const { userId, avatar, user } = messages[0];
        this.setState({
            isLoading: false,
            messages,
            user: {
                userId,
                avatar,
                user
            }
        });
    }
    onSend(message) {
        this.state.messages.push(message);
        this.setState({
            messages: this.state.messages
        })
    }
    setEditMessage(message) {
        this.setState({
            editMessage: message
        })
    }
    onEdit(updatedMessage) {
        const { id } = updatedMessage;
        const updatedMessages = this.state.messages.map(message => message.id === id ? Object.assign(message, updatedMessage) : message);
        this.setState({
            messages: updatedMessages,
            editMessage: null
        });
    }
    onDelete(id) {
        const updatedMessages = this.state.messages.filter(message => message.id !== id);
        this.setState({
            messages: updatedMessages
        });
    }
    onLike(updatedMessage) {
        const { id } = updatedMessage;
        const updatedMessages = this.state.messages.map(message => message.id === id ? Object.assign(message, updatedMessage) : message);
        this.setState({
            messages: updatedMessages
        });
    }
    render() {
        const { isLoading, messages, name, user, editMessage } = this.state;

        return (
            isLoading ? <Spinner /> :
                <div className="Page-container">
                    <PageHeader user={user} />
                    <div className="Chat">
                        <Header name={name} messages={messages} />
                        <MessageList messages={messages} userId={user.userId} onLike={this.onLike.bind(this)} onDelete={this.onDelete.bind(this)} setEditMessage={this.setEditMessage.bind(this)} />
                        {editMessage ? <EditMessageInput user={user} message={this.state.editMessage} onEdit={this.onEdit.bind(this)} /> :
                            <MessageInput user={user} onSend={this.onSend.bind(this)} editMessage={this.state.editMessage} />}
                    </div>
                    <PageFooter />
                </div>
        );
    }
}

export default Chat;