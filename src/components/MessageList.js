import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import moment from 'moment';
import '../App.css';
import TimeLIne from './TimeLine';
import Message from './Message';

class MessageList extends Component {
    constructor(props) {
        super(props);
        this.messagesEndRef = React.createRef();
    }

    scrollToBottom = () => {
        this.messagesEndRef.current.scrollIntoView({ behavior: 'smooth' })
    }

    groupMessagesByDays(messages) {
        const groupedResults = _.groupBy(messages, (message) => moment(new Date(message['createdAt'])).startOf('day'));
        return groupedResults;
    }
    getSortedDates(dates) {
        return dates.sort((date1, date2) => {
            const firstDate = new Date(date1);
            const secondDate = new Date(date2);
            return firstDate - secondDate;
        })
    }
    
    mapMessages(messagesByDate, userId) {
        const dates = Object.keys(messagesByDate);
        const sorted = this.getSortedDates(dates);
        return sorted.map((date) => {
            const messagesForOneDay = messagesByDate[date];
            const messageComponents = messagesForOneDay
                .map(message => <div key={message.id}><Message onLike={this.props.onLike} onDelete={this.props.onDelete} setEditMessage={this.props.setEditMessage} userId={userId} message={message} /></div>);

            return (
                <div key={date} id={date}>
                    <TimeLIne date={date} />
                    {messageComponents}
                </div>
            );
        });
    }
    render() {
        const { messages, userId } = this.props;
        const groupedMessages = this.groupMessagesByDays(messages);
        const mapped = this.mapMessages(groupedMessages, userId);
        return (
            <div id="block" className="MessageList card">
                {mapped}
            </div>
        );
    }
}

MessageList.propTypes = {
    messages: PropTypes.arrayOf(Object).isRequired,
    userId: PropTypes.string.isRequired,
    onLike: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    setEditMessage: PropTypes.func.isRequired
}

export default MessageList;